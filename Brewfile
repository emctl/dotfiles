# Generated via `brew bundle dump`

cask_args appdir: "/Applications"

# Tap Homebrew
tap "homebrew/bundle"
tap "homebrew/cask"
tap "homebrew/cask-fonts"
tap "homebrew/cask-versions"
tap "homebrew/core"
tap "homebrew/services"
tap "civo/tools"
tap "dapr/tap"

cask "obs"
cask "visual-studio-code"
cask "vagrant"
cask "raycast"
cask "lastpass"
cask "notion"
cask "rectangle"
cask "steam"
cask "iterm2"
cask "sensiblesidebuttons"

# General
brew "dockutil"

# System
brew "zsh-completions"
brew "mas"
brew "curl"
brew "wget"
brew "git"
brew "vim"
brew "openssl"
brew "coreutils"
brew "moreutils"
brew "findutils"
brew "binutils"
brew "rename"
brew "gnu-sed"
brew "gnu-tar"
brew "gawk"
brew "gnutls"
brew "gnu-indent"
brew "gnu-getopt"
brew "tree"
brew "htop"
brew "pidof"
brew "pstree"
brew "grep"
brew "tmux"
brew "openssh"
brew "rsync"
brew "ssh-copy-id"
brew "screen"
brew "gmp"
brew "nmap"
brew "socat"
brew "rlwrap"
brew "dnstracer"
brew "sslscan"
brew "watch"
brew "direnv"

# Ops
brew "rancher"
brew "awscli"
brew "ansible"
brew "google-cloud-sdk"
brew "jsonnet"
brew "jsonnet-bundler"
brew "tfenv"
brew "terraform-docs"
brew "civo"
brew "argocd"
brew "helm"
brew "helmfile"
brew "kustomize"
brew "kubernetes-cli"
brew "kubectx"
brew "kubevela"
brew "k9s"
brew "vault"
brew "dapr/tap/dapr-cli"
brew "rosa-cli"
brew "lima"

# Images, Audio, Video
brew "imagemagick"
brew "gifsicle"
brew "gifify"
brew "ffmpeg"

# Archive & Git
brew "xz"
brew "p7zip"
brew "git"
brew "git-lfs"
brew "tig"
brew "hub"
brew "gitdock"

# Extract rpm file content with rpm2cpio *.rpm | cpio -ivd
brew "rpm2cpio"

# JSON
brew "jq"
brew "jo"

# Dev
brew "ruby"
brew "yarn"
brew "rbenv"
brew "pipenv"
brew "pyenv"
brew "node"
brew "go"
brew "cmake"
brew "openjdk"
brew "rust"

# GitLab Pages
brew "hugo"

# App Store
mas "Slack", id: 803453959
mas "uBlock", id: 1385985095
mas "AdBlock", id: 1402042596
mas "iMovie", id: 408981434
mas "Instapaper", id: 288545208
mas "Instapaper Save", id: 1481302432
mas "Keynote", id: 409183694
mas "LastPass", id: 926036361
mas "Notion Web Clipper", id: 1559269364
mas "Numbers", id: 409203825
mas "Pages", id: 409201541
